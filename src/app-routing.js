import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import Login from './components/Login';
import { Register } from './components/Register';

const Home = () => {
  return <h1>Hi from home page.</h1>
}
const Contact = () => {
  return <h1>Hi from contact page.</h1>
}
const About = () => {
  return <h1>Hi from about page.</h1>
}
const Profile = () => {
  return <h1>Hi from profile page.</h1>
}
const Dashboard = () => {
  return <h1>Hi from dashboard page.</h1>
}
const PageNotFound = () => {
  return <h1>This page is not found in this website.</h1>
}
const Navigate = () => {
  return (
    <>
      <Link to = "/register">Register</Link>
      <Link to = "/login">Login</Link>
      <Link to = "/home">Home</Link>
      <Link to = "/profile">Profile</Link>
      <Link to = "/dashboard">Dashboard</Link>
      <Link to = "/contact">Contact</Link>
      <Link to = "/about">About</Link>
    </>
  );
}

// Stateless component
const Routing = (props) => {
  return (
    <Router>
      {/* Registration of route */}
      <Navigate />
      <Switch>
        <Route exact path = '/' component = {Register}></Route>
        <Route exact path = '/register' component = {Register}></Route>
        <Route path = '/login' component = {Login}></Route>
        <Route path = '/home' component = {Home}></Route>
        <Route path = '/contact' component = {Contact}></Route>
        <Route path = '/about' component = {About}></Route>
        <Route path = '/profile' component = {Profile}></Route>
        <Route path = '/dashboard' component = {Dashboard}></Route>
        <Route component = {PageNotFound}></Route>
      </Switch>
    </Router>
  )
}
export default Routing;