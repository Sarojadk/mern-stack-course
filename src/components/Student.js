import React, { Component } from 'react'
import Marks from './Marks';
export default class Student extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roll: 1
    };
  }
  clickHandle = () => {
    console.log(`Button clicked.`);
    // this.setState({
    //   roll: 4343
    // }); 
    this.setState({
      roll: this.state.roll + 2
    }); 
  }
  render() {
    console.log(`Student rendered.`)
    return (
      <div>
        <div>
          <Marks roll = {this.state.roll} />
          <button onClick = {this.clickHandle}>Change</button>
        </div>
      </div>
    );
  }
}
