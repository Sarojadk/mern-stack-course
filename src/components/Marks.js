import React, { Component } from 'react'

export default class Marks extends Component {
  constructor(props) {
    super(props); 
      this.state = {
        roll: this.props.roll
    };
  }
  static getDerivedStateFromProps(props, state) {
    console.log(props, state);
    console.log(`Get Derived State From Props`);
    if (props.roll !== state.roll) {
      return {roll: props.roll};
    }
      return null;
  }
  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log(`Marks GetSnapshotBeforeUpdate It runs before update.`);
    console.log(prevProps, prevState);
    return 45;
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.roll < 7) {
      console.log(`Marks should update.`);
      console.log(nextProps, nextState);
      return true;
    }
    console.log(nextProps, nextState);
    return false;
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log(`Marks component did update runs after update`)
    console.log(prevProps, prevState, snapshot);
  }
  
  
  render() {
    console.log(`Marks rendered.`);
    return (
      <div>
        <h1>{this.state.roll}</h1>
      </div>
    );
  }
}
