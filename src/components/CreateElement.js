import React, { Component } from 'react'
import Greet from './Greet'

export default class CreateElement extends Component {
  render() {
    console.log(`Create element rendered.`)
    return (
      <div>
        <h1>Hello {this.props.name}</h1>
        <Greet />
      </div>
    );
  }
}
