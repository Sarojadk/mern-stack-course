import React, { Component } from 'react';
import { Link } from 'react-router-dom'

const defaultForm = {
  firstName: '',
  lastName: '',
  email: '',
  phoneNumber: '',
  address: '',
  username: '',
  password: '',
  gender: '',
  dob: ''
}

export class Register extends Component {
  constructor() {
    super();
    this.state = {
      data: {
        ...defaultForm
      },
      error: {
        ...defaultForm
      },
      validForm: false,
      isSubmitting: false,
    }
    this.handleSubmit.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();
    this.setState({
      isSubmitting:true
      // http call (client-server communication for form data exchange.)
    });
    setTimeout(() => {
      this.setState({
        isSubmitting:false
      });
    }, 5000);
    console.log(`Form could not be submitted.`);

  }

  handleChange = (e) => {
    const { name, value } = e.target;
    console.log(`Name in the form field => ${name}`);
    console.log(`Value in the form field => ${value}`);
    this.setState((prevState) => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }), () => {
      this.validateErrors(name);
    });
    }
  validateErrors(fieldName) {
    let errorMsg;
    switch(fieldName) {
      case 'name':
        errorMsg = this.state.data[fieldName]
        ? ''
        : 'Name is required';
        break;
      case 'email':
        errorMsg = this.state.data[fieldName]
        ? this.state.data[fieldName].includes('@')
          ? ''
          : 'Not a valid email'
        : 'Email is required';
        break;

      default:
        errorMsg = '';
    }
    this.setState((errState) => ({
      error: {
        ...errState.error,
        [fieldName]: errorMsg
      }
    }), () => {
      console.log('State => ', this.state);
      this.validateForm();
    });
  }

  validateForm() {
    let btnEnabledStatus;
    const errors = Object
                  .values(this.state.error)
                  .filter(err => err);
    console.log('Errors in form input => ', errors);
    if(errors.length) {
      btnEnabledStatus = false;
    } else {
        btnEnabledStatus = false;
      }
      this.setState({
        validForm: btnEnabledStatus
      });
    }
  

  render() {
    let btn = this.state.isSubmitting
    ? <button disabled type ="submit">Submitting...</button>
    : <button disabled = {!this.state.validForm} type ="submit">Sign Up</button>

    return (
      <div className="Login">
        <header className="Login-header">
          <h2 className = "text-center">You are at register page</h2>
          <form action = "#" method = "POST" onSubmit = {this.handleSubmit} noValidate>
            <p>Create an account, it's easy and only takes a minute.</p>
            <div className = "half-width">
              <input type ="text" name ="firstName" id ="firstName" placeholder ="First Name" onChange = {this.handleChange} />
              <p id = "error">{this.state.error.firstName}</p>
              
              <input type ="text" name ="lastName" id ="lastName" placeholder ="Last Name" onChange = {this.handleChange} />
  
            </div>
            <input type = "email" name = "email" id = "email" placeholder = "Email Address" onChange = {this.handleChange} />
            <div className = "half-width">
              <input type ="password" name ="password" id ="password" placeholder ="Password" onChange = {this.handleChange} />
              <input type ="password" name ="reTypePassword" id ="reTypePassword" placeholder ="Re-enter Password" onChange = {this.handleChange} />
            </div>
            <div>
              <h4 className = "date-of-birth secondary-text">Date of birth:</h4>
              <select name = "dobDay" className = "date-of-birth-select" onChange = {this.handleChange} >
                <option value = "01">01</option>
                <option value = "02">02</option>
                <option value = "03">03</option>
                <option value = "04">04</option>
                <option value = "05">05</option>
                <option value = "06">06</option>
              </select>
              <select name = "dobMonth" className = "date-of-birth-select" onChange = {this.handleChange} >
                <option value = "01">01</option>
                <option value = "02">02</option>
                <option value = "03">03</option>
                <option value = "04">04</option>
                <option value = "05">05</option>
                <option value = "06">06</option>
              </select>
              <select name = "dobYear" className = "date-of-birth-select" onChange = {this.handleChange} >
                <option value = "2001">2000</option>
                <option value = "2002">2001</option>
                <option value = "2003">2002</option>
                <option value = "2004">2003</option>
                <option value = "2005">2004</option>
                <option value = "2006">2005</option>
              </select>
            </div>
            <div className = "gender-select">
              <h4 className = "secondary-text">Gender:</h4>
              <input type="radio" name="gender" id="male" value = "male" onChange = {this.handleChange} />
              <label htmlFor="male">Male</label>
              <input type="radio" name="gender" id="female"value = "female" onChange = {this.handleChange} />
              <label htmlFor="female">Female</label>
              <input type="radio" name="gender" id="custom" value = "custom" onChange = {this.handleChange} />
              <label htmlFor="custom">Custom</label>
            </div>
            {btn}
            <Link to = "/login">Already have an account Log In</Link>
          </form>
        </header>
    </div>
    );
  }
}
