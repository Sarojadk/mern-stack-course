import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {  Link } from 'react-router-dom'

import './Login.css';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state ={
      // count: 1,
      data: {
        firstName: null,
        lastName: null,
        password: null,
      },
      error: {
        firstName: null,
        lastName: null,
        password: null
      },
      validForm: false,
      isSubmitting: false
    }
    // console.log(`I am constructor block and I get executed at first in the mount phase of lifecycle.`);
  }
  /*
  componentWillMount() {
    console.log(`I am component will mount and I get executed before rendering.`);
  }
  
  componentDidMount() {
    this.interval = setInterval(() => {
      this.setState ({
        count: this.state['count'] + 1
      });
      
    }, 1000);
    console.log(`I am component did mount and I get executed after successful render.`);
  }
  
  componentDidUpdate(props, previousState) {
    console.log(`I am component did update block and I get executed whenever the state of the component updates or changes.`);
    console.log(`Previous props => ${props}`);
    console.log(`Previous state => ${previousState}`);
  }
  
  componentWillUnmount() {
    console.log(`Login component is going to be unmounted.`);
    clearInterval(this.interval);
    
  } 
  */
  
  handleChange(e) {
    const { name, value } = e.target;
    console.log(`Name in the form filed => ${name}`);
    console.log(`Value in the form filed => ${value}`);
    this.setState((previousState) => ({
      data: {
        ...previousState.data,
      [name]: value
      }
    }), () => this.validateForm(name));
    
  }
  
  validateForm(fieldName) {
    switch(fieldName) {
      case 'firstName':
        this.setState({
          firstNameErr : this.state.firstName ? '' : "First name is required."
        });
        break;
      case 'lastName':
        this.setState({
          lastNameErr : this.state.lastName ? '' : "Last name is required."
        });
        break;
      case 'password':
        this.setState({
          passwordErr : this.state.password ? '' : "Password is required."
        });
        break;
      default:
        break;
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    
    }
    // console.log(`You are about to be logging in... ${this.state}`);
  
  render() {
    // console.log(`I am render block and I get executed before rendering.`);
    return(
      <div className="Login">
        <header className="Login-header">
          <h2>Welcome to react app</h2>
          <form action = "#" method = "POST">
            <p>Please login to use this app.</p>
            <hr />
            {/* <p>Form submitted {this.state.count} times</p> */}
            <label htmlFor = "firstName">First Name:</label>
            <input type ="text" name ="firstName" id ="firstName" placeholder ="First Name" onChange = {this.handleChange.bind(this)} />
            <p id = "error">{this.state.firstNameErr}</p>

            <label htmlFor = "lastName">Last Name:</label>
            <input type ="text" name ="lastName" id ="lastName" placeholder ="Last Name" onChange = {this.handleChange.bind(this)} />
            <p id = "error">{this.state.lastNameErr}</p>

            <label htmlFor = "password">Password:</label>
            <input type ="password" name ="password" id ="password" placeholder ="Password" onChange ={this.handleChange.bind(this)} />
            <p id = "error">{this.state.passwordErr}</p>

            <button type ="submit" onClick = {this.handleSubmit}>Login</button>
            <Link to = '/register'>Don't have an account Register here</Link>
          </form>
        </header>
    </div>
    )
  }
}

// class Auth extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {};
//   }
//   mount() {
//     ReactDOM.render(<Login />, document.getElementById('test'));
//   }
//   unmount() {
//     ReactDOM.unmountComponentAtNode(document.getElementById('test'));
//   }
//   render() {
//     return (
//       <div>
//         <button onClick = {this.mount}>Mount</button>
//         <button onClick = {this.unmount}>Unount</button>
//         <div id = "test"></div>
//       </div>
//     )
//   }
// }

export default Login;
